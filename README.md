# localforge-sqlite-customdriver


Includes code from:

base64-arraybuffer
https://github.com/niklasvh/base64-arraybuffer

Copyright (c) 2012 Niklas von Hertzen
Licensed under the MIT license.

----------------------------------------

Based on https://github.com/mozilla/localForage/blob/master/src/drivers/websql.js
Copyright (c) 2015 Mozilla
Licensed under Apache 2.0 license.

Based on https://github.com/Ecostack/localForage-cordova-sqlite-plugin/blob/master/src/localforage-cordova-sqlite-plugin.js



# Install localforge-sqlite-customdriver

    `npm i @schluss/localforge-sqlite`


#Add Dependacies

    `npm install localforage`
    
    `cordova plugin add cordova-sqlite-storage `

#Include localforge-sqlite-customdriver

    `var cordovaSqliteplugin =require('@schluss/localforge-sqlite'); `
    
#Define Driver on app Startup
    
    `localforage.defineDriver(cordovaSqliteplugin);`

#Add driver into localforge.createInstance Function

    ```
    localforage.createInstance({
		name 		: 'schluss',
		version     : this.version,
		storeName   : name,
		driver: [
			cordovaSqliteplugin._driver,
			localforage.INDEXEDDB,
			localforage.WEBSQL,
			localforage.LOCALSTORAGE
		]
	});	
	
    ```
    
#Use localforge default methods to use database

    (https://github.com/localForage/localForage)
    (https://localforage.github.io/localForage/)

    
